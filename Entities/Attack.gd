extends Node


@onready var lightning = $lightning;
@onready var flash_image = $flash_sprite
@onready var flash_image_anim = $flash_sprite/AnimationPlayer;
@onready var lightning_sound = $lightning/AudioStreamPlayer;

func _ready():
	flash_image.modulate.a = 0;
	lightning.modulate.a = 0;

func play_flash():
	flash_image_anim.play("flash");
	lightning_sound.pitch_scale = 1 + (randf() * 0.5);
	lightning_sound.play();
	await flash_image_anim.animation_finished;
	flash_image_anim.play("RESET");
