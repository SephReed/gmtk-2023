extends CharacterBody2D


@onready var bass_anim = $Attack/Bass/AnimationPlayer;
@onready var camera = $Camera2D;
@onready var attack = $Attack;
@export var SPEED = 300.0;
@onready var timer_swing = $timer_swing;


	
func _input(event):
	if event.is_action_pressed("smash"):
		smash();


func _physics_process(delta):
	velocity.x = Input.get_axis("left", "right") * SPEED;
	velocity.y = Input.get_axis("up", "down") * SPEED;
	if (velocity.x != 0):
		attack.scale.x = -1 if velocity.x <= 0 else 1;
	move_and_slide()

func smash():
	bass_anim.play("swing");
	timer_swing.start(0.15);
	await timer_swing.timeout;
	attack.play_flash();
	camera.start_shake(0.25,.01,5);
	await bass_anim.animation_finished;	
	bass_anim.play("RESET");
