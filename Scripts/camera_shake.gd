extends Camera2D

@onready var timer_shake_length = $timer_shake_length;
#@onready var flash_image = $flash_sprite

var reset_speed = 0
var strength = 0
var doing_shake = false

#connect out timer signal timeouts
#func _ready():
#	flash_image.modulate.a = 0;
#	lightning.modulate.a = 0;
#	timer_wait_times.connect("timeout",self,"timeout_wait_times")
#	timer_shake_length.connect("timeout",self,"timeout_shake_length")
	
#This will stop the shake, and return camera offset to original value
#func timeout_shake_length():
#	doing_shake = false
#	reset_camera()
	
#This function does the tween shaking between intervals
func little_shake():
	if(!doing_shake):
		return;
		
	var tween = get_tree().create_tween();
	tween.set_ease(Tween.EASE_OUT);
	tween.set_trans(Tween.TRANS_SINE);
	
	var rand_dir = Vector2(randf_range(-strength,strength),randf_range(-strength,strength));
	strength = strength * 0.95;
	tween.tween_property(self, "offset", rand_dir, reset_speed);
	tween.tween_callback(self.little_shake);
	tween.play();
#	tween_shake.interpolate_property(self,"offset",offset, Vector2(rand_range(-strength,strength),rand_range(-strength,strength)),reset_speed,Tween.TRANS_SINE,Tween.EASE_OUT)
#	tween_shake.start()
		
#once we've finished shaking the screen, tween to original offset
func reset_camera():
	doing_shake = false;
	var tween = get_tree().create_tween();
	tween.set_ease(Tween.EASE_OUT);
	tween.set_trans(Tween.TRANS_SINE);
	tween.tween_property(self, "offset", Vector2(0,0), reset_speed);
	tween.play();
	
#we're telling the camera to start the shake, and pass some varibles to used in functions else where
func start_shake(time_of_shake,speed_of_shake,strength_of_shake):
	doing_shake = true
	strength = strength_of_shake
	reset_speed = speed_of_shake
	timer_shake_length.start(time_of_shake)
	little_shake();
	await timer_shake_length.timeout;
	reset_camera();
#	timer_wait_times.start(speed_of_shake)
	



